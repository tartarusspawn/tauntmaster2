
-- =================================================================================
--                          TauntMaster2                                          ==
--          http://www.curse.com/addons/wow/taunt-master-2                        ==
-- =================================================================================
-- Author      : Tartarusspawn
local L = LibStub("AceLocale-3.0"):NewLocale("TauntMaster2", "enUS", true)

L["Alpha (Dim) Level : (Player / Target)"] = true

L["Button Height"] = true
L["Button Width"] = true

L["Cataclysm"] = true
L["Config"]         = true

L["Display Options"] = true
L["Hide Friendly Target Border"] = true
L["Hide Low Mana Warning"] = true
L["Hide Tank Border"] = true
L["Hide Target = Tank's Target Border"] = true
L["Hide When Solo"] = true
L["Hides Friendly Target Border"] = true
L["Hides Low Mana Warning"] = true
L["Hides Tank Border"] = true
L["Hides Target = Tank's Target Border"] = "Hides Target = Tank's Target Border"
L["Hides window when solo"] = "Hides window when solo"
L["In Combat / In Range"] = "In Combat / In Range"
L["In Combat / Out of Range"] = "In Combat / Out of Range"

L["Left Button Options"]    = true
L["Lich King"] = "Lich King"
L["Lock In Place"] = "Lock In Place"
L["Locks Window In PLace"] = "Locks Window In PLace"

L["Mana Warning"] = "Mana Warning"
L["Max Columns"] = "Max Columns"
L["Mouse Butons"] 	= true

L["Options"] 		= true
L["Out of Combat / In Range"] = "Out of Combat / In Range"
L["Out of Combat / Out of  Range"] = "Out of Combat / Out of  Range"

L["Pandaria"] = "Pandaria"

L["Raid Warnings"] = "Raid Warnings"

L["Scale"]          = true
L["Show On Cooldown"] = "Show On Cooldown"
L["Show Taunts When on Cooldown"] = "Show Taunts When on Cooldown"
L["Show Taunts When When Ready"] = "Show Taunts When When Ready"
L["Show When Ready"] = "Show When Ready"
L["Spell Bindings"] = "Spell Bindings"

L["Taunt Cooldowns"] = "Taunt Cooldowns"

L["Taunt Master 2"] = true

L["Units Per Column"] = "Units Per Column"

L["Warlords"] = "Warlords"
















