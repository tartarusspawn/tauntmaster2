TM2debug = false
TMSchatSpam = true
-- =================================================================================
--                          TauntMaster2                                          ==
--          http://www.curse.com/addons/wow/taunt-master-2                        ==
-- =================================================================================
-- Author      : Tartarusspawn
	-- Print a message to the chat frame
	--self:Print("msg")
local TM2= select(2, ...)
TM2.version = GetAddOnMetadata("TauntMaster2", "Version")
-- register this file with Ace libraries

local TM2  = LibStub("AceAddon-3.0"):NewAddon(TM2,"TauntMaster2 ", "AceEvent-3.0", "AceConsole-3.0", "AceHook-3.0");
--TM2:SetDefaultModuleLibraries( "AceEvent-3.0", "AceConsole-3.0", "AceHook-3.0")

local private = {}
local L = LibStub("AceLocale-3.0"):GetLocale("TauntMaster2")

local TauntMasterSpells = {
	type = "spell",
	spell = "Auto Attack",
	unitsuffix = "",
	unit = "mouseover",
}


function TM2:OnInitialize()
	TM2:SysMessage("TM2:OnInitialize()")
end
	-- load the savedDB into TM2.db
	


function TM2:OnEnable()
	-- Called when the addon is enabled
wowversion = select(4, GetBuildInfo())
	-- Print a message to the chat frame
	TM2:SysMessage("TM2:OnEnable()")
	
	TM2.db = LibStub("AceDB-3.0"):New("TM2DB30025");
	TM2:LoadVariables()


	for name, module in pairs(TM2.modules) do
		TM2[name] = module
	end	
	local Util = TM2:GetModule("Util")
	local RaidFrames = TM2:GetModule("RaidFrames")

	SLASH_TAUNTMASTER_SLASH_COMMAND1 = "/tauntmaster";
	SLASH_TAUNTMASTER_SLASH_COMMAND2 = "/tm";
	local function handler(msg, editbox)
		if msg == 'display' then
			--TMConfig:Show();
		elseif msg == 'spells' then
			Util:DebugSpellTabInfo()
			--TauntMasterMouseButtons:Show();
		elseif msg == 'show' then
			--TauntMaster_Header:Show();
			--TauntMasterDBChar.hideTM = nil
		elseif msg == 'hide' then
			--TauntMaster_Header:Hide();
			--TauntMasterDBChar.hideTM = 1
		elseif msg == 'toggle' then
			--if TauntMasterDBChar.hideTM == 1 then
			--TauntMaster_Header:Show();
			--TauntMasterDBChar.hideTM = nil
			--elseif TauntMasterDBChar.hideTM == nil then
			--TauntMaster_Header:Hide();
			--TauntMasterDBChar.hideTM = 1
			--end
		elseif msg == 'raid' or msg == 'warn' then
			--TMTauntWarnFrame:Show()
		elseif msg == 'debug' then
--TMTauntWarnFrame:Show()
		else
			if (TM2_OptionsFrame:IsVisible()) then
				TM2_OptionsFrame:Hide()
			else
				TM2_OptionsFrame:Show()
			end
		end
	end
	SlashCmdList["TAUNTMASTER_SLASH_COMMAND"] = handler;

	----------------------------------------------------------------------------
	-- Minimap Button functions
	----------------------------------------------------------------------------

	local TM2MinimapBtn = CreateFrame("BUTTON", "tm2Minimap",Minimap)
	TM2MinimapBtn:SetWidth(33)
	TM2MinimapBtn:SetHeight(33)
	TM2MinimapBtn:SetClampedToScreen(true)
	TM2MinimapBtn:SetPoint("TOPLEFT")
	local overlay = TM2MinimapBtn:CreateTexture(nil, "OVERLAY")
	overlay:SetSize(53, 53)
	overlay:SetTexture("Interface\\Minimap\\MiniMap-TrackingBorder")
	overlay:SetPoint("TOPLEFT")
	local background = TM2MinimapBtn:CreateTexture(nil, "BACKGROUND")
	background:SetSize(20, 20)
	background:SetTexture("Interface\\Minimap\\UI-Minimap-Background")
	background:SetPoint("TOPLEFT", 7, -5)	
	local icon = TM2MinimapBtn:CreateTexture(nil, "ARTWORK")
	icon:SetSize(17, 17)
	icon:SetTexture("Interface\\AddOns\\TauntMaster2\\Icons\\tga\\tmicon")
	icon:SetPoint("TOPLEFT", 7, -6)
	TM2MinimapBtn:SetMovable(true)
	TM2MinimapBtn:EnableMouse(true)
	TM2MinimapBtn:RegisterForDrag("")
	TM2MinimapBtn:RegisterForClicks("AnyUp")
	TM2MinimapBtn:SetScript("OnDragStart", TM2MinimapBtn.StartMoving)
	TM2MinimapBtn:SetScript("OnDragStop", TM2MinimapBtn.StopMovingOrSizing)
	TM2MinimapBtn:SetScript("OnClick", function(self, button2) 
		if (button2 =="LeftButton" ) then
			if ( TauntMasterUnitFrames_Header:IsVisible() ) then
				TauntMasterUnitFrames_Header:Hide()
			else
				TauntMasterUnitFrames_Header:Show()
			end
		elseif (button2 =="RightButton" ) then
			if ( TM2_OptionsFrame:IsVisible() ) then
				TM2_OptionsFrame:Hide()
			else
				TM2_OptionsFrame:Show()
			end
		end
	end)
	TM2MinimapBtn:SetScript("OnDragStart", TM2MinimapBtn.StartMoving)
	
	TM2MinimapBtn:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_CURSOR")
		GameTooltip:AddLine("Taunt Master 2",1,1,1)
		GameTooltip:AddLine(" ")
		--GameTooltip:AddDoubleLine(L["Left Click"], L["Toggle Minitax Frame"], 0,1,0, 1,1,1);
		--GameTooltip:AddDoubleLine(L["Right Click"], L["Opens Interface Options"], 0,1,0, 1,1,1);
		GameTooltip:Show()
    end)

    TM2MinimapBtn:SetScript("OnLeave", function() GameTooltip:Hide() end)
	
	TM2MinimapBtn:SetAlpha(1)
	TM2MinimapBtn:Show()
end



function TM2:OnDisable()
	TM2:SysMessage("TM2:OnDisable()")
	-- Called when the addon is disabled
end

function TM2:SysMessage(msg)
	if(TM2debug) then
		self:Print("Debug: "..msg);
	end
end

function TM2:GDHchatSpam(msg)
	if(TMSchatSpam) then
		self:Print(msg);
	end
end






function TM2:LoadVariables()
	TM2:SysMessage("TM2:LoadVariables()")
	local currentSpec			= GetSpecialization()
	local currentSpecName		= currentSpec and select(2, GetSpecializationInfo(currentSpec)) or "WARRIOR"
	TM2.db.char.class			= TM2.db.char.class or select(2, UnitClass("player")) or "WARRIOR"
	TM2.db.char.specialization	= currentSpecName
	-- ======================
	-- GUI
	-- ======================
	TM2.db.char.gui 				= TM2.db.char.gui or {}
	TM2.db.char.gui.maxColumns		= TM2.db.char.gui.maxColumns or 8
	TM2.db.char.gui.unitsPerColumn	= TM2.db.char.gui.unitsPerColumn or 5
	TM2.db.char.gui.width			= TM2.db.char.gui.width or 63
	TM2.db.char.gui.height			= TM2.db.char.gui.height or 32
	TM2.db.char.gui.scale			= TM2.db.char.gui.scale or 1
	TM2.db.char.gui.positon			= TM2.db.char.gui.position or {}

	TM2.db.char.gui.showRaid	= TM2.db.char.gui.showRaid or true
	TM2.db.char.gui.showParty	= TM2.db.char.gui.showParty or true
	TM2.db.char.gui.showPlayer	= TM2.db.char.gui.showPlayer or true
	TM2.db.char.gui.showSolo	= TM2.db.char.gui.showSolo or true

	TM2.db.char.gui.groupBy			= TM2.db.char.gui.groupBy or "ASSIGNEDROLE"
	TM2.db.char.gui.groupingOrder	= TM2.db.char.gui.groupingOrder or "MAINTANK, MAINASSIST, TANK, HEALER, DAMAGER, NONE"
	-- ======================
	-- Spells
	-- ======================
	TM2.db.char.Spells					= TM2.db.char.Spells or {}
	TM2.db.char.Spells[currentSpecName]	= TM2.db.char.Spells[currentSpecName] or {}

	TM2.db.char.Spells[currentSpecName]["TM2Button1"] = TM2.db.char.Spells[currentSpecName]["TM2Button1"]  or {}
	TM2.db.char.Spells[currentSpecName]["TM2Button2"] = TM2.db.char.Spells[currentSpecName]["TM2Button2"]  or {}
	TM2.db.char.Spells[currentSpecName]["TM2Button3"] = TM2.db.char.Spells[currentSpecName]["TM2Button3"]  or {}
	TM2.db.char.Spells[currentSpecName]["TM2Button4"] = TM2.db.char.Spells[currentSpecName]["TM2Button4"]  or {}
	TM2.db.char.Spells[currentSpecName]["TM2Button5"] = TM2.db.char.Spells[currentSpecName]["TM2Button5"]  or {}

	-- Actionbutton1
	TM2.db.char.Spells[currentSpecName]["TM2Button1"][0] 	= TM2.db.char.Spells[currentSpecName]["TM2Button1"][0]  or TauntMasterSpells	-- Left Button
	TM2.db.char.Spells[currentSpecName]["TM2Button1"][1] 	= TM2.db.char.Spells[currentSpecName]["TM2Button1"][1]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button1"][2] 	= TM2.db.char.Spells[currentSpecName]["TM2Button1"][2]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button1"][3] 	= TM2.db.char.Spells[currentSpecName]["TM2Button1"][3]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button1"][4] 	= TM2.db.char.Spells[currentSpecName]["TM2Button1"][4]  or TauntMasterSpells

	-- Actionbutton2
	TM2.db.char.Spells[currentSpecName]["TM2Button2"][0] 	= TM2.db.char.Spells[currentSpecName]["TM2Button2"][0]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button2"][1] 	= TM2.db.char.Spells[currentSpecName]["TM2Button2"][1]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button2"][2] 	= TM2.db.char.Spells[currentSpecName]["TM2Button2"][2]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button2"][3] 	= TM2.db.char.Spells[currentSpecName]["TM2Button2"][3]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button2"][4] 	= TM2.db.char.Spells[currentSpecName]["TM2Button2"][4]  or TauntMasterSpells

	-- Actionbutton3
	TM2.db.char.Spells[currentSpecName]["TM2Button3"][0] 	= TM2.db.char.Spells[currentSpecName]["TM2Button3"][0]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button3"][1] 	= TM2.db.char.Spells[currentSpecName]["TM2Button3"][1]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button3"][2] 	= TM2.db.char.Spells[currentSpecName]["TM2Button3"][2]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button3"][3] 	= TM2.db.char.Spells[currentSpecName]["TM2Button3"][3]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button3"][4] 	= TM2.db.char.Spells[currentSpecName]["TM2Button3"][4]  or TauntMasterSpells

	-- Actionbutton4
	TM2.db.char.Spells[currentSpecName]["TM2Button4"][0] 	= TM2.db.char.Spells[currentSpecName]["TM2Button4"][0]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button4"][1] 	= TM2.db.char.Spells[currentSpecName]["TM2Button4"][1]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button4"][2] 	= TM2.db.char.Spells[currentSpecName]["TM2Button4"][2]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button4"][3] 	= TM2.db.char.Spells[currentSpecName]["TM2Button4"][3]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button4"][4] 	= TM2.db.char.Spells[currentSpecName]["TM2Button4"][4]  or TauntMasterSpells

	-- Actionbutton5
	TM2.db.char.Spells[currentSpecName]["TM2Button5"][0] 	= TM2.db.char.Spells[currentSpecName]["TM2Button5"][0]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button5"][1] 	= TM2.db.char.Spells[currentSpecName]["TM2Button5"][1]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button5"][2] 	= TM2.db.char.Spells[currentSpecName]["TM2Button5"][2]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button5"][3] 	= TM2.db.char.Spells[currentSpecName]["TM2Button5"][3]  or TauntMasterSpells
	TM2.db.char.Spells[currentSpecName]["TM2Button5"][4] 	= TM2.db.char.Spells[currentSpecName]["TM2Button5"][4]  or TauntMasterSpells

end



function TM2MinimapBtn_Hide()
	TM2MinimapBtn:Hide()
end

function TM2MinimapBtn_show()
	TM2MinimapBtn:Show()
end
