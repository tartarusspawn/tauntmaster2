## Author: Tartarusspawn
## Interface: 60200
## Title: Taunt Master 2
## Version: 30019
## SavedVariables: TM2DebugLog, wowversion 
## SavedVariablesPerCharacter: TM2DB30025
embeds.xml
Locale\enUS.lua
TauntMaster2.lua
Gui\Util.lua
Gui\Options.lua
Gui\RaidFrame.lua
Gui\BarUI.lua

Gui\Options.xml
Gui\BarUI.xml
Gui\RaidFrame.xml
