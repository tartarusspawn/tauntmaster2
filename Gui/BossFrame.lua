local L = LibStub("AceLocale-3.0"):GetLocale("TauntMaster2") -- loads the localization table
-- =================================================================================
--                          TauntMaster2                                          ==
--          http://www.curse.com/addons/wow/taunt-master-2                        ==
-- =================================================================================
-- Author      : Tartarusspawn
	-- Print a message to the chat frame
	--self:Print("msg")
local TM2 = select(2, ...)
local Options =  TM2:NewModule("Options")
local Util = TM2:GetModule("Util")
local Spacer = "    "
local private = {}

CreateFrame("Frame", "TestHeader", UIParent, "SecureHandlerStateTemplate")



TestHeader:SetPoint("CENTER")

local nextButton = 1

function CreateTestButton()
  -- Create the button
  local button = CreateFrame(
    "CheckButton",
    "TestButton"..nextButton,
    TestHeader,
    "ActionBarButtonTemplate"
  )
  button:SetAttribute("useparent-statebutton", true)
  
  -- Add the button to the header 
  TestHeader:SetAttribute("addchild", button)
  
  -- Attach buttons beyond the first to the previous one
  if nextButton > 1 then
    button:SetPoint("LEFT", _G["TestButton"..(nextButton - 1)], "RIGHT")
  end
  
  -- Move the first button to the left to keep all the buttons centered
  local offsetX = - (nextButton - 1) * button:GetWidth() / 2
  TestButton1:SetPoint("CENTER", TestHeader, "CENTER", offsetX, 0)
  
  nextButton = nextButton + 1
  return button
end

CreateTestButton()



local ActionBars = {'Action','MultiBarBottomLeft','MultiBarBottomRight','MultiBarRight','MultiBarLeft'}

2.function PrintActions()

3.    for _, barName in pairs(ActionBars) do

4.        for i = 1, 12 do

5.            local button = _G[barName .. 'Button' .. i]

6.            local slot = ActionButton_GetPagedID(button) or ActionButton_CalculateAction(button) or button:GetAttribute('action') or 0

7.            if HasAction(slot) then

8.                local actionName, _

9.                local actionType, id = GetActionInfo(slot)

10.                if actionType == 'macro' then _, _ , id = GetMacroSpell(id) end

11.                if actionType == 'item' then

12.                    actionName = GetItemInfo(id)

13.                elseif actionType == 'spell' or (actionType == 'macro' and id) then

14.                    actionName = GetSpellInfo(id)

15.                end

16.                if actionName then

17.                    print(button:GetName(), actionType, (GetSpellLink(id)), actionName)

18.                end

19.            end

20.        end

21.    end

22.end

