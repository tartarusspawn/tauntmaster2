local L = LibStub("AceLocale-3.0"):GetLocale("TauntMaster2") -- loads the localization table
-- =================================================================================
--                          TauntMaster2                                          ==
--          http://www.curse.com/addons/wow/taunt-master-2                        ==
-- =================================================================================
-- Author      : Tartarusspawn
	-- Print a message to the chat frame
	--TM2:Print("msg")
local TM2 = select(2, ...)
local Util =  TM2:NewModule("Util", "AceEvent-3.0", "AceConsole-3.0", "AceHook-3.0")
local private = {}

function Util:OnInitialize()
	-- do init tasks here, like loading the Saved Variables, 
	-- or setting up slash commands.
end

function Util:OnEnable()
	-- Do more initialization here, that really enables the use of your addon.
	-- Register Events, Hook functions, Create Frames, Get information from 
	-- the game that wasn't available in OnInitialize
end

function Util:OnDisable()
	-- Unhook, Unregister Events, Hide frames that you created.
	-- You would probably only use an OnDisable if you want to 
	-- build a "standby" mode, or be able to toggle modules on/off.
end

function Util:OnInitialize()
	-- do init tasks here, like loading the Saved Variables, 
	-- or setting up slash commands.
end

function Util:SysMessage(msg)
	if(TM2debug) then
		self:Print("SYS Msg: "..msg);
	end
end

function Util:GDHchatSpam(msg)
	if(TMSchatSpam) then
		self:Print(msg);
	end
end

function Util:Spell_initialize(self, level, SpellNameVar, TB_AB_State,ABar1)
	local info 
	local currentSpecName = TM2.db.char.specialization
	local tm2numTabs = GetNumSpellTabs()
	for BookTabs = 1,tm2numTabs do
		local name,texture,offset,numSpells = GetSpellTabInfo(BookTabs)
		if BookTabs == 1 or name == currentSpecName  then
			if level == 1 then
				info = UIDropDownMenu_CreateInfo()
				info.text = (name .. " Submenu")
				info.value = name
				info.isTitle = true
				info.hasArrow = true
				UIDropDownMenu_AddButton(info, level)
			end
		end
		for BookSpells = offset + 1,offset + numSpells do
			local skillName = GetSpellBookItemName(BookSpells, "spell")
			local skillNameid = select(7, GetSpellInfo(BookSpells, "spell"))
			if BookTabs <=5 then
				if (IsAttackSpell(BookSpells, "spell"))  then
				end
			end
			if ( IsHarmfulSpell(BookSpells, "spell")) or (IsHelpfulSpell(BookSpells, "spell") ) then 
				if not (IsPassiveSpell(BookSpells, "spell") ) then
					if level == 2 and UIDROPDOWNMENU_MENU_VALUE == name then
						info = UIDropDownMenu_CreateInfo()
						info.text = skillName
						if info.text == TM2.db.char.Spells[currentSpecName][SpellNameVar][TB_AB_State]["spell"] then
							info.checked = true
						else
							info.checked = false
						end
						function info.func(button)
							TM2.db.char.Spells[currentSpecName][SpellNameVar][TB_AB_State]["spell"] = skillName
							if not ( IsHarmfulSpell(skillName) ) then 
								TM2.db.char.Spells[currentSpecName][SpellNameVar][TB_AB_State]["unitsuffix"] = ""
							else 
								TM2.db.char.Spells[currentSpecName][SpellNameVar][TB_AB_State]["unitsuffix"]= "target"

							end
							--Set Dropdown text
							UIDropDownMenu_SetText(self, skillName)
							--Set TM2 Action Button

							--SpellNameVar:SetAttribute("*unitsuffix" .. (TB_AB_State + 1), TM2.db.char.Spells[currentSpecName][SpellNameVar][TB_AB_State]["unitsuffix"])
							ABar1:SetState(TB_AB_State, "spell", select(7, GetSpellInfo(skillName)))
						end
						UIDropDownMenu_AddButton(info, level)
					end
				end
			end
		end
	end
	--[[
	local numAccountMacros, numCharacterMacros = GetNumMacros()
	if level == 1 then
	info = UIDropDownMenu_CreateInfo()
	info.text = ("Account Macros Submenu")
	info.value = "Account Macros"
	info.isTitle = true
	info.hasArrow = true
	UIDropDownMenu_AddButton(info, level)

	info = UIDropDownMenu_CreateInfo()
	info.text = ("CharacterMacros Submenu")
	info.value = "CharacterMacros"
	info.isTitle = true
	info.hasArrow = true
	UIDropDownMenu_AddButton(info, level)

	end
	
	for MacroInfo =1,numAccountMacros do
	local name = select(1,GetMacroInfo(MacroInfo) )
	if level == 2 and UIDROPDOWNMENU_MENU_VALUE == "Account Macros" then
	info = UIDropDownMenu_CreateInfo()
	info.text = name
	--info.isTitle = true
	if info.text == TM2.db.char.Spells[TM2.db.char.id][tm2buttonname] then
	info.checked = true
	else
	info.checked = false
	end
	function info.func(button)
	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname] = name
	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname .. "Macro"] = true

	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname .. "Mod"] = "target"

	UIDropDownMenu_SetText(self, TM2.db.char.Spells[TM2.db.char.id][tm2buttonname])
	end
	UIDropDownMenu_AddButton(info, level)
	end
	end
	]]
end

function Util:CreateActionButton(name, header, nextButton)
	-- Create the button
	local button = CreateFrame(
		"CheckButton",
		name..nextButton,
		header,
		ActionBarButtonTemplate,
		nextButton
		)
	--button:SetAttribute("useparent-statebutton", true)
	button:SetAttribute("type", "spell")
	button:SetAttribute("spell", "Death Grip")
	button:SetAttribute("texture", GetSpellBookItemTexture("Death Grip"))
	--button:SetAttribute("useparent-statebutton", true)
	button:SetID(nextButton)
	--Add the button to the header 
	header:SetAttribute("addchild", button)
	-- Attach buttons beyond the first to the previous one
	if nextButton > 1 then
		button:SetPoint("LEFT", _G[name..(nextButton - 1)], "RIGHT")
	else
		-- Move the first button to the left to keep all the buttons centered
		--local offsetX = - (nextButton - 1) * button:GetWidth() / 2
		button:SetPoint("CENTER", header, "CENTER")
	end

	return button
end

function Util:DebugSpellTabInfo()
Util:GDHchatSpam("*** Exporting spells to saved varibles ***" )
	local tm2numTabs = GetNumSpellTabs()
	for BookTabs = 1,tm2numTabs do
		local name,texture,offset,numSpells = GetSpellTabInfo(BookTabs)
		
		for BookSpells = offset + 1,offset + numSpells do
			skillname = GetSpellBookItemName(BookSpells, "spell")
			TM2DebugSpells= TM2DebugSpells or {}
			TM2DebugSpells.Spellbook = TM2DebugSpells.Spellbook or {}
			TM2DebugSpells.Spellbook[name] = TM2DebugSpells.Spellbook[name] or {}
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"]	= TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"] or {}
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname] = TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname] or {}
			--skillType, spellId = GetSpellBookItemInfo(index, "bookType") or GetSpellBookItemInfo("spellName")
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["skillType"]		= select(1,GetSpellBookItemInfo(BookSpells, "spell"))
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["icon"]			= select(2,GetSpellBookItemInfo(BookSpells, "spell"))
			-- name, rank, icon, castingTime, minRange, maxRange, spellID = GetSpellInfo(index, "bookType") or GetSpellInfo("name") or GetSpellInfo(id)
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["rank"]			= select(2,GetSpellInfo(BookSpells, "spell"))
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["icon"]			= select(3,GetSpellInfo(BookSpells, "spell"))
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["castingTime"]	= select(4,GetSpellInfo(BookSpells, "spell"))
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["minRange"]		= select(5,GetSpellInfo(BookSpells, "spell"))
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["maxRange"]		= select(6,GetSpellInfo(BookSpells, "spell"))
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["spellID"]		= select(7,GetSpellInfo(BookSpells, "spell"))
			--isHarmful = IsHarmfulSpell(index, "bookType") or IsHarmfulSpell("name")
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["isHarmful"]			= IsHarmfulSpell(BookSpells, "spell")
			--isHarmful = IsHelpfulSpell(index, "bookType") or IsHelpfulSpell("name")
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["IsHelpfulSpell"]	= IsHelpfulSpell(BookSpells, "spell")
			--isPassive = IsPassiveSpell(index, "bookType") or IsPassiveSpell("name")
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["IsPassiveSpell"]	= IsPassiveSpell(BookSpells, "spell")
			--isAttack = IsAttackSpell(index, "bookType") or IsAttackSpell("name")
			TM2DebugSpells.Spellbook[name]["GetSpellBookItemName"][skillname]["IsAttackSpell"]	= IsAttackSpell(BookSpells, "spell")
		end
	end
end

