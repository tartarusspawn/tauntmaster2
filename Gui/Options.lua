local L = LibStub("AceLocale-3.0"):GetLocale("TauntMaster2") -- loads the localization table
-- =================================================================================
--                          TauntMaster2                                          ==
--          http://www.curse.com/addons/wow/taunt-master-2                        ==
-- =================================================================================
-- Author      : Tartarusspawn

local TM2 = select(2, ...)
local Options =  TM2:NewModule("Options", "AceConsole-3.0", "AceHook-3.0")
local Util = TM2:GetModule("Util")
local Spacer = "    "
local private = {}
local maxtabs = 2
local currentSpecName = ""

function Options:OnInitialize()
	Options:SysMessage("Options:OnInitialize()")
end

function Options:OnEnable()
	Options:SysMessage("Options:OnEnable()")
	private.CreateFrame(TM2_OptionsFrame)
	

end

function Options:OnDisable()

end

function Options:SysMessage(msg)
	if(TM2debug) then
		self:Print("Debug: "..msg);
	end
end

function Options:ChatSpam(msg)
	if(TMSchatSpam) then
		self:Print(msg);
	end
end

function TM2_Frame_ButtonHandler(self)
	Options:ChatSpam(self:GetName())

	local button_id = self:GetID()
	if  button_id == 201 then 
		PanelTemplates_SetTab(TM2_OptionsFrame, 1)
		TM2_TabPage1:Show()
		TM2_TabPage2:Hide()
	elseif button_id == 202 then
		PanelTemplates_SetTab(TM2_OptionsFrame, 2);
		TM2_TabPage1:Hide();
		TM2_TabPage2:Show();

		TM2ButtonPage1:Show();
		TM2ButtonPage2:Hide();
		TM2ButtonPage3:Hide();
		TM2ButtonPage4:Hide();
		TM2ButtonPage5:Hide();
	elseif button_id == 211 then
		--PanelTemplates_SetTab(TM2_TabPage2, 2);
		TM2_TabPage1:Hide();
		TM2_TabPage2:Show();

		TM2ButtonPage1:Show();
		TM2ButtonPage2:Hide();
		TM2ButtonPage3:Hide();
		TM2ButtonPage4:Hide();
		TM2ButtonPage5:Hide();
	elseif button_id == 212 then
		TM2_TabPage1:Hide();
		TM2_TabPage2:Show();

		TM2ButtonPage1:Hide();
		TM2ButtonPage2:Show();
		TM2ButtonPage3:Hide();
		TM2ButtonPage4:Hide();
		TM2ButtonPage5:Hide();
	elseif button_id == 213 then
		TM2_TabPage1:Hide();
		TM2_TabPage2:Show();

		TM2ButtonPage1:Hide();
		TM2ButtonPage2:Hide();
		TM2ButtonPage3:Show();
		TM2ButtonPage4:Hide();
		TM2ButtonPage5:Hide();
	elseif button_id == 214 then
		TM2_TabPage1:Hide();
		TM2_TabPage2:Show();

		TM2ButtonPage1:Hide();
		TM2ButtonPage2:Hide();
		TM2ButtonPage3:Hide();
		TM2ButtonPage4:Show();
		TM2ButtonPage5:Hide();
	elseif button_id == 215 then
		TM2_TabPage1:Hide();
		TM2_TabPage2:Show();

		TM2ButtonPage1:Hide();
		TM2ButtonPage2:Hide();
		TM2ButtonPage3:Hide();
		TM2ButtonPage4:Hide();
		TM2ButtonPage5:Show();
	end
end

function private.CreateFrame(frame)
	Options:SysMessage("private.CreateFrame(frame)")
	frame:Hide()
	frame:SetClampedToScreen(true)
	frame.TitleText:SetText(L["Taunt Master 2"]);
	frame:CreateTitleRegion()
	frame:RegisterForDrag("LeftButton")
	SetPortraitToTexture(frame:GetName() .. "Portrait", "Interface\\TARGETINGFRAME\\targetdead")

	local TutorialButton =  CreateFrame("Button","TM2_OptionsFrameTutorialButton", frame, "MainHelpPlateButton")
	TutorialButton:SetPoint("TOPLEFT", frame, 39, 20)


	frame:SetScript("OnDragStart", frame.StartMoving)
	frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
	frame:SetScript("OnShow", function()
		private.TM2_OptionsFrame_OnShow(frame)
	end)
	TutorialButton:SetScript("OnClick", function()
		private.TM2_OptionsFrame_ToggleTutorial();
	end)

	private.CreateDisplayOptions(TM2_TabPage1)

	private.CreateLeftOptions(TM2ButtonPage1)
	private.CreateRightOptions(TM2ButtonPage2)
	private.CreateMiddleOptions(TM2ButtonPage3)
	private.CreateButton4Options(TM2ButtonPage4)
	private.CreateButton5Options(TM2ButtonPage5)
end

-- =======================================================
-- Display Options
-- =======================================================
function private.CreateDisplayOptions(frame)


	
	local Title = frame:CreateFontString (nil, "ARTWORK", "GameFontNormalLarge")
	Title:SetPoint ("TOP", 0, -16)
	Title:SetText (L["Display Options"])

	local S1 = CreateFrame("Slider", "TM2 Scale" , frame, "OptionsSliderTemplate")
	S1:ClearAllPoints()
	S1:SetWidth (200)
	S1:SetPoint ("TOPLEFT" ,8 , -70)
	S1:SetMinMaxValues(1, 200)
	S1:SetValueStep(1)
	S1:SetObeyStepOnDrag(true)
	S1:SetValue(TM2.db.char.gui.scale * 100)
	_G["TM2 Scale" .."Low"]:SetText("1");
	_G["TM2 Scale" .."High"]:SetText("200");
	_G["TM2 Scale" .."Text"]:SetText(L["Taunt Master 2"] .. " " .. L["Scale"] ..":  ".. TM2.db.char.gui.scale * 100 .. " %");

	S1:SetScript("OnValueChanged", function(self,tm2scalevalue)
		TM2.db.char.gui.scale=tm2scalevalue /100
		TauntMasterUnitFrames_Header:SetScale(TM2.db.char.gui.scale)
		_G["TM2 Scale" .."Text"]:SetText("TM2 Scale" ..":  ".. TM2.db.char.gui.scale * 100 .. " %" );
	end);
	
	--TM2.db.char.gui.maxColumns
	local S2 = CreateFrame("Slider", "maxcolumns" , frame, "OptionsSliderTemplate")
	S2:ClearAllPoints()
	S2:SetWidth (200)
	S2:SetPoint ("TOPLEFT" ,8 , -110)
	S2:SetMinMaxValues(1, 40)
	S2:SetValueStep(1)
	S2:SetObeyStepOnDrag(true)
	S2:SetValue(TM2.db.char.gui.maxColumns)
	_G["maxcolumns" .."Low"]:SetText("1");
	_G["maxcolumns" .."High"]:SetText("40");
	_G["maxcolumns" .."Text"]:SetText(L["Max Columns"] ..":  ".. TM2.db.char.gui.maxColumns );

	S2:SetScript("OnValueChanged", function(self,tm2maxcolumns)
		TM2.db.char.gui.maxColumns = tm2maxcolumns
		TauntMasterUnitFrames_Header:SetAttribute("maxColumns", TM2.db.char.gui.maxColumns)
		_G["maxcolumns" .."Text"]:SetText(L["Max Columns"] ..":  ".. TM2.db.char.gui.maxColumns )
	end);

	--TM2.db.char.gui.unitsPerColumn
	local S3 = CreateFrame("Slider", "unitspercolumn" , frame, "OptionsSliderTemplate")
	S3:ClearAllPoints()
	S3:SetWidth (200)
	S3:SetPoint ("TOPLEFT" ,8 , -150)
	S3:SetMinMaxValues(1, 40)
	S3:SetValueStep(1)
	S3:SetObeyStepOnDrag(true)
	S3:SetValue(TM2.db.char.gui.unitsPerColumn)
	_G["unitspercolumn" .."Low"]:SetText("1");
	_G["unitspercolumn" .."High"]:SetText("40");
	_G["unitspercolumn" .."Text"]:SetText(L["Units Per Column"] ..":  ".. TM2.db.char.gui.unitsPerColumn );

	S3:SetScript("OnValueChanged", function(self,unitspercolumn)
		TM2.db.char.gui.unitsPerColumn= unitspercolumn
TauntMasterUnitFrames_Header:SetAttribute("maxColumns", 1)
		TauntMasterUnitFrames_Header:SetAttribute("unitsPerColumn" , TM2.db.char.gui.unitsPerColumn)
TauntMasterUnitFrames_Header:SetAttribute("maxColumns", TM2.db.char.gui.maxColumns)
		_G["unitspercolumn" .."Text"]:SetText(L["Units Per Column"] ..":  ".. TM2.db.char.gui.unitsPerColumn )
	end);



end


-- =======================================================
-- LeftButton Options
-- =======================================================
function private.CreateLeftOptions(frame)
	Options:SysMessage("private.CreateLeftOptions")
	local currentSpecName = TM2.db.char.specialization
	local GuiLeftLoc = -16
	local Title = frame:CreateFontString (nil, "ARTWORK", "GameFontNormalLarge")
	Title:SetPoint ("TOP", 0, GuiLeftLoc)
	Title:SetText (L["Left Button Options"])

	GuiLeftLoc = -75
	local LeftButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	LeftButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiLeftLoc)
	LeftButtonDropDowntext:SetText ("Left Button Config")
	LeftButtonDropDowntext:SetWidth (500)
	LeftButtonDropDowntext:SetJustifyH ("LEFT")
	GuiLeftLoc = GuiLeftLoc - 25
	if not LeftButtonDropDown then 
		CreateFrame("FRAME", "LeftButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	LeftButtonDropDown:ClearAllPoints()
	LeftButtonDropDown:SetPoint("TOPLEFT", 8, GuiLeftLoc)
	LeftButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(LeftButtonDropDown, private.Left_Spell_initialize)
	UIDropDownMenu_SetWidth(LeftButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(LeftButtonDropDown, 124)
	UIDropDownMenu_SetText(LeftButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button1"][0]["spell"])
	GuiLeftLoc = GuiLeftLoc - 50
	
	local AltLeftButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	AltLeftButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiLeftLoc)
	AltLeftButtonDropDowntext:SetText ("ALT Left Button Config")
	AltLeftButtonDropDowntext:SetWidth (500)
	AltLeftButtonDropDowntext:SetJustifyH ("LEFT")
	frame.AltLeftButtonDropDowntext = AltLeftButtonDropDowntext
	GuiLeftLoc = GuiLeftLoc - 25
	if not AltLeftButtonDropDown then 
		CreateFrame("FRAME", "AltLeftButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	AltLeftButtonDropDown:ClearAllPoints()
	AltLeftButtonDropDown:SetPoint("TOPLEFT", 8, GuiLeftLoc)
	AltLeftButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(AltLeftButtonDropDown, private.AltLeft_Spell_initialize)
	UIDropDownMenu_SetWidth(AltLeftButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(AltLeftButtonDropDown, 124)
	UIDropDownMenu_SetText(AltLeftButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button1"][1]["spell"])
	GuiLeftLoc = GuiLeftLoc - 50
	
	local ShiftLeftButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	ShiftLeftButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiLeftLoc)
	ShiftLeftButtonDropDowntext:SetText ("Shift Left Button Config")
	ShiftLeftButtonDropDowntext:SetWidth (500)
	ShiftLeftButtonDropDowntext:SetJustifyH ("LEFT")
	frame.ShiftLeftButtonDropDowntext = ShiftLeftButtonDropDowntext
	GuiLeftLoc = GuiLeftLoc - 25
	if not ShiftLeftButtonDropDown then 
		CreateFrame("FRAME", "ShiftLeftButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	ShiftLeftButtonDropDown:ClearAllPoints()
	ShiftLeftButtonDropDown:SetPoint("TOPLEFT", 8, GuiLeftLoc)
	ShiftLeftButtonDropDown:Show()
	UIDropDownMenu_Initialize(ShiftLeftButtonDropDown, private.ShiftLeft_Spell_initialize)
	UIDropDownMenu_SetWidth(ShiftLeftButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(ShiftLeftButtonDropDown, 124)
	UIDropDownMenu_SetText(ShiftLeftButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button1"][2]["spell"])
	GuiLeftLoc = GuiLeftLoc - 50
	
	local CtrlLeftButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	CtrlLeftButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiLeftLoc)
	CtrlLeftButtonDropDowntext:SetText ("Ctrl Left Button Config")
	CtrlLeftButtonDropDowntext:SetWidth (500)
	CtrlLeftButtonDropDowntext:SetJustifyH ("LEFT")
	frame.CtrlLeftButtonDropDowntext = CtrlLeftButtonDropDowntext
	GuiLeftLoc = GuiLeftLoc - 25
	if not CtrlLeftButtonDropDown then 
		CreateFrame("FRAME", "CtrlLeftButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end

	CtrlLeftButtonDropDown:ClearAllPoints()
	CtrlLeftButtonDropDown:SetPoint("TOPLEFT", 8, GuiLeftLoc)
	CtrlLeftButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(CtrlLeftButtonDropDown, private.CtrlLeft_Spell_initialize)
	UIDropDownMenu_SetWidth(CtrlLeftButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(CtrlLeftButtonDropDown, 124)
	UIDropDownMenu_SetText(CtrlLeftButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button1"][3]["spell"])

end


function private.Left_Spell_initialize(self,level)
	
	private.Spell_initialize(self,level,"TM2Button1",0 , TM2_ActionButton1, TM2_player_ActionButton1)
end

function private.AltLeft_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button1",1 , TM2_ActionButton1, TM2_player_ActionButton1)
end

function private.ShiftLeft_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button1",2 , TM2_ActionButton1, TM2_player_ActionButton1)
end

function private.CtrlLeft_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button1",3 , TM2_ActionButton1, TM2_player_ActionButton1)
end

-- =======================================================
-- RightButton Options
-- =======================================================
function private.CreateRightOptions(frame)
	local currentSpecName = TM2.db.char.specialization
	local GuiRightLoc = -16
	
	local Title = frame:CreateFontString (nil, "ARTWORK", "GameFontNormalLarge")
	Title:SetPoint ("TOP", 0, GuiRightLoc)
	Title:SetText ("Right Button Options")
	GuiRightLoc = -75
	
	local RightButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	RightButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiRightLoc)
	RightButtonDropDowntext:SetText ("Right Button Config")
	RightButtonDropDowntext:SetWidth (500)
	RightButtonDropDowntext:SetJustifyH ("Left")
	frame.RightButtonDropDowntext = RightButtonDropDowntext
	GuiRightLoc = GuiRightLoc - 25
	if not RightButtonDropDown then 
		CreateFrame("FRAME", "RightButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	RightButtonDropDown:ClearAllPoints()
	RightButtonDropDown:SetPoint("TOPLEFT", 8, GuiRightLoc)
	RightButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(RightButtonDropDown, private.Right_Spell_initialize)
	UIDropDownMenu_SetWidth(RightButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(RightButtonDropDown, 124)
	UIDropDownMenu_SetText(RightButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button2"][0]["spell"])
	GuiRightLoc = GuiRightLoc - 50
	
	local AltRightButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	AltRightButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiRightLoc)
	AltRightButtonDropDowntext:SetText ("ALT Right Button Config")
	AltRightButtonDropDowntext:SetWidth (500)
	AltRightButtonDropDowntext:SetJustifyH ("Left")
	frame.AltRightButtonDropDowntext = AltRightButtonDropDowntext
	GuiRightLoc = GuiRightLoc - 25
	if not AltRightButtonDropDown then 
		CreateFrame("FRAME", "AltRightButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	AltRightButtonDropDown:ClearAllPoints()
	AltRightButtonDropDown:SetPoint("TOPLEFT", 8, GuiRightLoc)
	AltRightButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(AltRightButtonDropDown, private.AltRight_Spell_initialize)
	UIDropDownMenu_SetWidth(AltRightButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(AltRightButtonDropDown, 124)
	UIDropDownMenu_SetText(AltRightButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button2"][1]["spell"])
	GuiRightLoc = GuiRightLoc - 50
	
	local ShiftRightButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	ShiftRightButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiRightLoc)
	ShiftRightButtonDropDowntext:SetText ("Shift Right Button Config")
	ShiftRightButtonDropDowntext:SetWidth (500)
	ShiftRightButtonDropDowntext:SetJustifyH ("Left")
	frame.ShiftRightButtonDropDowntext = ShiftRightButtonDropDowntext
	GuiRightLoc = GuiRightLoc - 25
	if not ShiftRightButtonDropDown then 
		CreateFrame("FRAME", "ShiftRightButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	ShiftRightButtonDropDown:ClearAllPoints()
	ShiftRightButtonDropDown:SetPoint("TOPLEFT", 8, GuiRightLoc)
	ShiftRightButtonDropDown:Show()
	UIDropDownMenu_Initialize(ShiftRightButtonDropDown, private.ShiftRight_Spell_initialize)
	UIDropDownMenu_SetWidth(ShiftRightButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(ShiftRightButtonDropDown, 124)
	UIDropDownMenu_SetText(ShiftRightButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button2"][2]["spell"])
	GuiRightLoc = GuiRightLoc - 50
	
	local CtrlRightButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	CtrlRightButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiRightLoc)
	CtrlRightButtonDropDowntext:SetText ("Ctrl Right Button Config")
	CtrlRightButtonDropDowntext:SetWidth (500)
	CtrlRightButtonDropDowntext:SetJustifyH ("Left")
	frame.CtrlRightButtonDropDowntext = CtrlRightButtonDropDowntext
	GuiRightLoc = GuiRightLoc - 25
	if not CtrlRightButtonDropDown then 
		CreateFrame("FRAME", "CtrlRightButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end

	CtrlRightButtonDropDown:ClearAllPoints()
	CtrlRightButtonDropDown:SetPoint("TOPLEFT", 8, GuiRightLoc)
	CtrlRightButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(CtrlRightButtonDropDown, private.CtrlRight_Spell_initialize)
	UIDropDownMenu_SetWidth(CtrlRightButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(CtrlRightButtonDropDown, 124)
	UIDropDownMenu_SetText(CtrlRightButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button2"][3]["spell"])
end

function private.Right_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button2",0 , TM2_ActionButton2, TM2_player_ActionButton2)
end

function private.AltRight_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button2",1 , TM2_ActionButton2, TM2_player_ActionButton2)
end

function private.ShiftRight_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button2",2 , TM2_ActionButton2, TM2_player_ActionButton2)
end

function private.CtrlRight_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button2",3 , TM2_ActionButton2, TM2_player_ActionButton2)
end

-- =======================================================
-- MiddleButton	
-- =======================================================
function private.CreateMiddleOptions(frame)
	local currentSpecName = TM2.db.char.specialization
	local GuiMiddleLoc = -16
	
	local Title = frame:CreateFontString (nil, "ARTWORK", "GameFontNormalLarge")
	Title:SetPoint ("TOP", 0, GuiMiddleLoc)
	Title:SetText ("Middle Button Options")
	GuiMiddleLoc = -75
	
	local MiddleButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	MiddleButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiMiddleLoc)
	MiddleButtonDropDowntext:SetText ("Middle Button Config")
	MiddleButtonDropDowntext:SetWidth (500)
	MiddleButtonDropDowntext:SetJustifyH ("Left")
	frame.MiddleButtonDropDowntext = MiddleButtonDropDowntext
	GuiMiddleLoc = GuiMiddleLoc - 25
	if not MiddleButtonDropDown then 
		CreateFrame("FRAME", "MiddleButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	MiddleButtonDropDown:ClearAllPoints()
	MiddleButtonDropDown:SetPoint("TOPLEFT", 8, GuiMiddleLoc)
	MiddleButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(MiddleButtonDropDown, private.Middle_Spell_initialize)
	UIDropDownMenu_SetWidth(MiddleButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(MiddleButtonDropDown, 124)
	UIDropDownMenu_SetText(MiddleButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button3"][0]["spell"])
	GuiMiddleLoc = GuiMiddleLoc - 50
	
	local AltMiddleButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	AltMiddleButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiMiddleLoc)
	AltMiddleButtonDropDowntext:SetText ("ALT Middle Button Config")
	AltMiddleButtonDropDowntext:SetWidth (500)
	AltMiddleButtonDropDowntext:SetJustifyH ("Left")
	frame.AltMiddleButtonDropDowntext = AltMiddleButtonDropDowntext
	GuiMiddleLoc = GuiMiddleLoc - 25
	if not AltMiddleButtonDropDown then 
		CreateFrame("FRAME", "AltMiddleButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	AltMiddleButtonDropDown:ClearAllPoints()
	AltMiddleButtonDropDown:SetPoint("TOPLEFT", 8, GuiMiddleLoc)
	AltMiddleButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(AltMiddleButtonDropDown, private.AltMiddle_Spell_initialize)
	UIDropDownMenu_SetWidth(AltMiddleButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(AltMiddleButtonDropDown, 124)
	UIDropDownMenu_SetText(AltMiddleButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button3"][1]["spell"])
	GuiMiddleLoc = GuiMiddleLoc - 50
	
	local ShiftMiddleButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	ShiftMiddleButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiMiddleLoc)
	ShiftMiddleButtonDropDowntext:SetText ("Shift Middle Button Config")
	ShiftMiddleButtonDropDowntext:SetWidth (500)
	ShiftMiddleButtonDropDowntext:SetJustifyH ("Left")
	frame.ShiftMiddleButtonDropDowntext = ShiftMiddleButtonDropDowntext
	GuiMiddleLoc = GuiMiddleLoc - 25
	if not ShiftMiddleButtonDropDown then 
		CreateFrame("FRAME", "ShiftMiddleButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	ShiftMiddleButtonDropDown:ClearAllPoints()
	ShiftMiddleButtonDropDown:SetPoint("TOPLEFT", 8, GuiMiddleLoc)
	ShiftMiddleButtonDropDown:Show()
	UIDropDownMenu_Initialize(ShiftMiddleButtonDropDown, private.ShiftMiddle_Spell_initialize)
	UIDropDownMenu_SetWidth(ShiftMiddleButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(ShiftMiddleButtonDropDown, 124)
	UIDropDownMenu_SetText(ShiftMiddleButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button3"][2]["spell"])
	GuiMiddleLoc = GuiMiddleLoc - 50
	
	local CtrlMiddleButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	CtrlMiddleButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiMiddleLoc)
	CtrlMiddleButtonDropDowntext:SetText ("Ctrl Middle Button Config")
	CtrlMiddleButtonDropDowntext:SetWidth (500)
	CtrlMiddleButtonDropDowntext:SetJustifyH ("Left")
	frame.CtrlMiddleButtonDropDowntext = CtrlMiddleButtonDropDowntext
	GuiMiddleLoc = GuiMiddleLoc - 25
	if not CtrlMiddleButtonDropDown then 
		CreateFrame("FRAME", "CtrlMiddleButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end

	CtrlMiddleButtonDropDown:ClearAllPoints()
	CtrlMiddleButtonDropDown:SetPoint("TOPLEFT", 8, GuiMiddleLoc)
	CtrlMiddleButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(CtrlMiddleButtonDropDown, private.CtrlMiddle_Spell_initialize)
	UIDropDownMenu_SetWidth(CtrlMiddleButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(CtrlMiddleButtonDropDown, 124)
	UIDropDownMenu_SetText(CtrlMiddleButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button3"][3]["spell"])
end

function private.Middle_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button3",0 , TM2_ActionButton3, TM2_player_ActionButton3)
end

function private.AltMiddle_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button3",1 , TM2_ActionButton3, TM2_player_ActionButton3)
end

function private.ShiftMiddle_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button3",2 , TM2_ActionButton3, TM2_player_ActionButton3)
end

function private.CtrlMiddle_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button3",3 , TM2_ActionButton3, TM2_player_ActionButton3)
end
	
-- =======================================================
-- Button4Button	
-- =======================================================
function private.CreateButton4Options(frame)
	local currentSpecName = TM2.db.char.specialization
	local GuiButton4Loc = -16
	
	local Title = frame:CreateFontString (nil, "ARTWORK", "GameFontNormalLarge")
	Title:SetPoint ("TOP", 0, GuiButton4Loc)
	Title:SetText ("Button 4 Options")
	GuiButton4Loc =  -75
	
	local Button4ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	Button4ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	Button4ButtonDropDowntext:SetText ("Button4 Button Config")
	Button4ButtonDropDowntext:SetWidth (500)
	Button4ButtonDropDowntext:SetJustifyH ("Left")
	frame.Button4ButtonDropDowntext = Button4ButtonDropDowntext
	GuiButton4Loc = GuiButton4Loc - 25
	if not Button4ButtonDropDown then 
		CreateFrame("FRAME", "Button4ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	Button4ButtonDropDown:ClearAllPoints()
	Button4ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	Button4ButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(Button4ButtonDropDown, private.Button4_Spell_initialize)
	UIDropDownMenu_SetWidth(Button4ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(Button4ButtonDropDown, 124)
	UIDropDownMenu_SetText(Button4ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button4"][0]["spell"])
	GuiButton4Loc = GuiButton4Loc - 50
	
	local AltButton4ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	AltButton4ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	AltButton4ButtonDropDowntext:SetText ("ALT Button4 Button Config")
	AltButton4ButtonDropDowntext:SetWidth (500)
	AltButton4ButtonDropDowntext:SetJustifyH ("Left")
	frame.AltButton4ButtonDropDowntext = AltButton4ButtonDropDowntext
	GuiButton4Loc = GuiButton4Loc - 25
	if not AltButton4ButtonDropDown then 
		CreateFrame("FRAME", "AltButton4ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	AltButton4ButtonDropDown:ClearAllPoints()
	AltButton4ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	AltButton4ButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(AltButton4ButtonDropDown, private.AltButton4_Spell_initialize)
	UIDropDownMenu_SetWidth(AltButton4ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(AltButton4ButtonDropDown, 124)
	UIDropDownMenu_SetText(AltButton4ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button4"][1]["spell"])
	GuiButton4Loc = GuiButton4Loc - 50
	
	local ShiftButton4ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	ShiftButton4ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	ShiftButton4ButtonDropDowntext:SetText ("Shift Button4 Button Config")
	ShiftButton4ButtonDropDowntext:SetWidth (500)
	ShiftButton4ButtonDropDowntext:SetJustifyH ("Left")
	frame.ShiftButton4ButtonDropDowntext = ShiftButton4ButtonDropDowntext
	GuiButton4Loc = GuiButton4Loc - 25
	if not ShiftButton4ButtonDropDown then 
		CreateFrame("FRAME", "ShiftButton4ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	ShiftButton4ButtonDropDown:ClearAllPoints()
	ShiftButton4ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	ShiftButton4ButtonDropDown:Show()
	UIDropDownMenu_Initialize(ShiftButton4ButtonDropDown, private.ShiftButton4_Spell_initialize)
	UIDropDownMenu_SetWidth(ShiftButton4ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(ShiftButton4ButtonDropDown, 124)
	UIDropDownMenu_SetText(ShiftButton4ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button4"][2]["spell"])
	GuiButton4Loc = GuiButton4Loc - 50
	
	local CtrlButton4ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	CtrlButton4ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	CtrlButton4ButtonDropDowntext:SetText ("Ctrl Button4 Button Config")
	CtrlButton4ButtonDropDowntext:SetWidth (500)
	CtrlButton4ButtonDropDowntext:SetJustifyH ("Left")
	frame.CtrlButton4ButtonDropDowntext = CtrlButton4ButtonDropDowntext
	GuiButton4Loc = GuiButton4Loc - 25
	if not CtrlButton4ButtonDropDown then 
		CreateFrame("FRAME", "CtrlButton4ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end

	CtrlButton4ButtonDropDown:ClearAllPoints()
	CtrlButton4ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton4Loc)
	CtrlButton4ButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(CtrlButton4ButtonDropDown, private.CtrlButton4_Spell_initialize)
	UIDropDownMenu_SetWidth(CtrlButton4ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(CtrlButton4ButtonDropDown, 124)
	UIDropDownMenu_SetText(CtrlButton4ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button4"][3]["spell"])
end
		
function private.Button4_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button4",0 , TM2_ActionButton4, TM2_player_ActionButton4)
end

function private.AltButton4_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button4",1 , TM2_ActionButton4, TM2_player_ActionButton4)
end

function private.ShiftButton4_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button4",2 , TM2_ActionButton4, TM2_player_ActionButton4)
end

function private.CtrlButton4_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button4",3 , TM2_ActionButton4, TM2_player_ActionButton4)
end

-- =======================================================
-- Button5Button	
-- =======================================================
function private.CreateButton5Options(frame)
	local currentSpecName = TM2.db.char.specialization
	local GuiButton5Loc = -16
	
	local Title = frame:CreateFontString (nil, "ARTWORK", "GameFontNormalLarge")
	Title:SetPoint ("TOP", 0, GuiButton5Loc)
	Title:SetText ("Button 5 Options")
	GuiButton5Loc =  -75
	
	local Button5ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	Button5ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	Button5ButtonDropDowntext:SetText ("Button5 Button Config")
	Button5ButtonDropDowntext:SetWidth (500)
	Button5ButtonDropDowntext:SetJustifyH ("Left")
	frame.Button5ButtonDropDowntext = Button5ButtonDropDowntext
	GuiButton5Loc = GuiButton5Loc - 25
	if not Button5ButtonDropDown then 
		CreateFrame("FRAME", "Button5ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	Button5ButtonDropDown:ClearAllPoints()
	Button5ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	Button5ButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(Button5ButtonDropDown, private.Button5_Spell_initialize)
	UIDropDownMenu_SetWidth(Button5ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(Button5ButtonDropDown, 124)
	UIDropDownMenu_SetText(Button5ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button5"][0]["spell"])
	GuiButton5Loc = GuiButton5Loc - 50
	
	local AltButton5ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	AltButton5ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	AltButton5ButtonDropDowntext:SetText ("ALT Button5 Button Config")
	AltButton5ButtonDropDowntext:SetWidth (500)
	AltButton5ButtonDropDowntext:SetJustifyH ("Left")
	frame.AltButton5ButtonDropDowntext = AltButton5ButtonDropDowntext
	GuiButton5Loc = GuiButton5Loc - 25
	if not AltButton5ButtonDropDown then 
		CreateFrame("FRAME", "AltButton5ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	AltButton5ButtonDropDown:ClearAllPoints()
	AltButton5ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	AltButton5ButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(AltButton5ButtonDropDown, private.AltButton5_Spell_initialize)
	UIDropDownMenu_SetWidth(AltButton5ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(AltButton5ButtonDropDown, 124)
	UIDropDownMenu_SetText(AltButton5ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button5"][0]["spell"])
	GuiButton5Loc = GuiButton5Loc - 50
	
	local ShiftButton5ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	ShiftButton5ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	ShiftButton5ButtonDropDowntext:SetText ("Shift Button5 Button Config")
	ShiftButton5ButtonDropDowntext:SetWidth (500)
	ShiftButton5ButtonDropDowntext:SetJustifyH ("Left")
	frame.ShiftButton5ButtonDropDowntext = ShiftButton5ButtonDropDowntext
	GuiButton5Loc = GuiButton5Loc - 25
	if not ShiftButton5ButtonDropDown then 
		CreateFrame("FRAME", "ShiftButton5ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end
	ShiftButton5ButtonDropDown:ClearAllPoints()
	ShiftButton5ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	ShiftButton5ButtonDropDown:Show()
	UIDropDownMenu_Initialize(ShiftButton5ButtonDropDown, private.ShiftButton5_Spell_initialize)
	UIDropDownMenu_SetWidth(ShiftButton5ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(ShiftButton5ButtonDropDown, 124)
	UIDropDownMenu_SetText(ShiftButton5ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button5"][0]["spell"])
	GuiButton5Loc = GuiButton5Loc - 50
	
	local CtrlButton5ButtonDropDowntext = frame:CreateFontString (nil, "ARTWORK", "GameFontHighlight")
	CtrlButton5ButtonDropDowntext:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	CtrlButton5ButtonDropDowntext:SetText ("Ctrl Button5 Button Config")
	CtrlButton5ButtonDropDowntext:SetWidth (500)
	CtrlButton5ButtonDropDowntext:SetJustifyH ("Left")
	frame.CtrlButton5ButtonDropDowntext = CtrlButton5ButtonDropDowntext
	GuiButton5Loc = GuiButton5Loc - 25
	if not CtrlButton5ButtonDropDown then 
		CreateFrame("FRAME", "CtrlButton5ButtonDropDown",  frame , "UIDropDownMenuTemplate")
	end

	CtrlButton5ButtonDropDown:ClearAllPoints()
	CtrlButton5ButtonDropDown:SetPoint ("TOPLEFT", 8, GuiButton5Loc)
	CtrlButton5ButtonDropDown:Show()
	
	UIDropDownMenu_Initialize(CtrlButton5ButtonDropDown, private.CtrlButton5_Spell_initialize)
	UIDropDownMenu_SetWidth(CtrlButton5ButtonDropDown, 125)
	UIDropDownMenu_SetButtonWidth(CtrlButton5ButtonDropDown, 124)
	UIDropDownMenu_SetText(CtrlButton5ButtonDropDown, TM2.db.char.Spells[currentSpecName]["TM2Button5"][0]["spell"])
	
end

function private.Button5_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button5",0 , TM2_ActionButton5, TM2_player_ActionButton5)
end

function private.AltButton5_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button5",1 , TM2_ActionButton5, TM2_player_ActionButton5)
end

function private.ShiftButton5_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button5",2 , TM2_ActionButton5, TM2_player_ActionButton5)
end

function private.CtrlButton5_Spell_initialize(self,level)
	private.Spell_initialize(self,level,"TM2Button5",3 , TM2_ActionButton5, TM2_player_ActionButton5)
end

function private.TM2_OptionsFrame_OnLoad(frame)
	Options:SysMessage("private.TM2_OptionsFrame_OnLoad")


end

function  private.TM2_OptionsFrame_OnShow(self, event, ...)
	Options:SysMessage("TM2_OptionsFrame_OnShow")
	private.TM2_OptionsFrame_Update()
end

function  private.TM2_OptionsFrame_OnEvent(self, event, ...)
	Options:SysMessage("private.TM2_OptionsFrame_OnEvent")
end

function  private.TM2_OptionsFrame_Update()
	Options:SysMessage("TM2_OptionsFrame_Update")
end

-- Tutorial Features
function private.TM2_OptionsFrame_ToggleTutorial()
	Options:ChatSpam("Tutorial Features Not Implemented Yet")
end

function private.TM2_OptionsFrame_GetTutorialEnum()
	--[[
	local helpPlate;
	local tutorial;
	if ( TM2_OptionsFrame.TABTYPE == TABTYPE_ACTIONBUTTONS ) then
	helpPlate = TM2_OptionsFrame_HelpPlate;
	tutorial = LE_FRAME_TUTORIAL_TM2_Options;
	elseif ( TM2_OptionsFrame.TABTYPE == TABTYPE_PROFESSION ) then
	helpPlate = ProfessionsFrame_HelpPlate;
	tutorial = LE_FRAME_TUTORIAL_PROFESSIONS;
	elseif ( TM2_OptionsFrame.TABTYPE == TABTYPE_CORE_ABILITIES ) then
	helpPlate = CoreAbilitiesFrame_HelpPlate;
	tutorial = LE_FRAME_TUTORIAL_CORE_ABILITITES;
	end
	return tutorial, helpPlate;
	]]
end




function private.Spell_initialize(self,level,tm2buttonname, State, TM2_AB, TM2_AB2)
	Options:SysMessage("private.Spell_initialize")
	local info 
	local tm2numTabs = GetNumSpellTabs()
	for BookTabs = 1,tm2numTabs do
		local name,texture,offset,numSpells = GetSpellTabInfo(BookTabs)
		local currentSpec = GetSpecialization()
		local currentSpecName = currentSpec and select(2, GetSpecializationInfo(currentSpec)) or select(1, UnitClass("player"))
		Options:SysMessage("line 317 : " .. currentSpecName)
		if BookTabs == 1 or name == currentSpecName  then
			if level == 1 then
				info = UIDropDownMenu_CreateInfo()
				info.text = (name .. " Submenu")
				info.value = name
				info.isTitle = true
				info.hasArrow = true
				Options:SysMessage("line 324 : " .. info.text)
				UIDropDownMenu_AddButton(info, level)
				Options:SysMessage("line 327 : " .. info.value)
			end
		end
		for BookSpells = offset + 1,offset + numSpells do
			local skillName = GetSpellBookItemName(BookSpells, "spell")
			local skillNameid = select(7, GetSpellInfo(BookSpells, "spell"))
			if BookTabs <=5 then
				if (IsAttackSpell(BookSpells, "spell"))  then
				end
			end
			--if ((IsHarmfulSpell(BookSpells, "spell") or IsHelpfulSpell(BookSpells, "spell")) and not (IsPassiveSpell(BookSpells, "spell"))) or (IsAttackSpell(BookSpells, "spell"))  then 
			if (not (IsPassiveSpell(BookSpells, "spell"))) or (IsAttackSpell(BookSpells, "spell"))  then 
				--if (IsSpellKnown(skillNameid )) then
				if level == 2 and UIDROPDOWNMENU_MENU_VALUE == name then
					info = UIDropDownMenu_CreateInfo()
					info.text = skillName
					if info.text ==   TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["spell"]then
						--TM2.db.char.Spells[currentSpecName]["TM2Button"][0]["spell"]
						info.checked = true
					else
						info.checked = false
					end
					function info.func()
						TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["spell"] = skillName
						TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["type"] = "spell"
						if not ( IsHarmfulSpell(skillName) ) then 
							TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["unitsuffix"] = ""
						else 
							TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["unitsuffix"] = "target"

						end
						--Set Dropdown text
						UIDropDownMenu_SetText(self, TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["spell"])
						--Set TM2 Action Button
						
						TM2_AB:SetAttribute("*unitsuffix" .. (State + 1), TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["unitsuffix"])
						TM2_AB:SetState(State, "spell", select(7, GetSpellInfo(TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["spell"])))
						TM2_AB2:SetState(State, "spell", select(7, GetSpellInfo(TM2.db.char.Spells[currentSpecName][tm2buttonname][State]["spell"])))
					end
					UIDropDownMenu_AddButton(info, level)
				end
				--end
			end
		end
	end
	local numAccountMacros, numCharacterMacros = GetNumMacros()
	if level == 1 then
		info = UIDropDownMenu_CreateInfo()
		info.text = ("Account Macros Submenu")
		info.value = "Account Macros"
		info.isTitle = true
		info.hasArrow = true
		UIDropDownMenu_AddButton(info, level)

		info = UIDropDownMenu_CreateInfo()
		info.text = ("CharacterMacros Submenu")
		info.value = "CharacterMacros"
		info.isTitle = true
		info.hasArrow = true
		UIDropDownMenu_AddButton(info, level)
		
	end
	--[[
	for MacroInfo =1,numAccountMacros do
	local name = select(1,GetMacroInfo(MacroInfo) )
	if level == 2 and UIDROPDOWNMENU_MENU_VALUE == "Account Macros" then
	info = UIDropDownMenu_CreateInfo()
	info.text = name
	--info.isTitle = true
	if info.text == TM2.db.char.Spells[TM2.db.char.id][tm2buttonname] then
	info.checked = true
	else
	info.checked = false
	end
	function info.func(button)
	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname] = name
	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname .. "Macro"] = true

	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname .. "Mod"] = "target"

	UIDropDownMenu_SetText(self, TM2.db.char.Spells[TM2.db.char.id][tm2buttonname])
	end
	UIDropDownMenu_AddButton(info, level)
	end
	end

	for i =1, numCharacterMacros do
	local cname = select(1,GetMacroInfo(MAX_ACCOUNT_MACROS +i ) )
	if level == 2 and UIDROPDOWNMENU_MENU_VALUE == "CharacterMacros" then
	info = UIDropDownMenu_CreateInfo()
	info.text = cname
	--info.isTitle = true
	if info.text == TM2.db.char.Spells[TM2.db.char.id][tm2buttonname] then
	info.checked = true
	else
	info.checked = false
	end
	function info.func(button)
	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname] = cname
	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname .. "Macro"] = true

	TM2.db.char.Spells[TM2.db.char.id][tm2buttonname .. "Mod"] = "target"

	UIDropDownMenu_SetText(self, TM2.db.char.Spells[TM2.db.char.id][tm2buttonname])
	end
	UIDropDownMenu_AddButton(info, level)
	end
	end
	]]
end

function private.Set_Actionbutton_Spells(button, button2, action, spell)

	button1:SetState(0, AB_Spells[0]["type"], select(7, GetSpellInfo(AB_Spells[0]["spell"])))
	button1:SetState(1, AB_Spells[1]["type"], select(7, GetSpellInfo(AB_Spells[1]["spell"])))
	button1:SetState(2, AB_Spells[2]["type"], select(7, GetSpellInfo(AB_Spells[2]["spell"])))
	button1:SetState(3, AB_Spells[3]["type"], select(7, GetSpellInfo(AB_Spells[3]["spell"])))
	
	button2:SetState(0, AB_Spells[0]["type"], select(7, GetSpellInfo(AB_Spells[0]["spell"])))
	button2:SetState(1, AB_Spells[1]["type"], select(7, GetSpellInfo(AB_Spells[1]["spell"])))
	button2:SetState(2, AB_Spells[2]["type"], select(7, GetSpellInfo(AB_Spells[2]["spell"])))
	button2:SetState(3, AB_Spells[3]["type"], select(7, GetSpellInfo(AB_Spells[3]["spell"])))

end


