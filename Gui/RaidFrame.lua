-- local L = LibStub("AceLocale-3.0"):GetLocale("TauntMaster2")
-- =================================================================================
--                          TauntMaster2                                          ==
--          http://www.curse.com/addons/wow/taunt-master-2                        ==
-- =================================================================================
-- Author      : Tartarusspawn
	-- Print a message to the chat frame
	--self:Print("msg")
	
local TM2 = select(2, ...)
local RaidFrames =  TM2:NewModule("RaidFrames", "AceEvent-3.0", "AceConsole-3.0", "AceHook-3.0")
local private = {}

-- This function can return a substring of a UTF-8 string, properly handling
-- UTF-8 codepoints.  Rather than taking a start index and optionally an end
-- index, it takes the string, the start index and the number of characters
-- to select from the string.
--
-- UTF-8 Reference:
-- 0xxxxxx - ASCII character
-- 110yyyxx - 2 byte UTF codepoint
-- 1110yyyy - 3 byte UTF codepoint
-- 11110zzz - 4 byte UTF codepoint

local function utf8sub(str, start, numChars)
	local currentIndex = start
	while numChars > 0 and currentIndex <= #str do
		local char = string.byte(str, currentIndex)
		if char >= 240 then
			currentIndex = currentIndex + 4
		elseif char >= 225 then
			currentIndex = currentIndex + 3
		elseif char >= 192 then
			currentIndex = currentIndex + 2
		else
			currentIndex = currentIndex + 1
		end
		numChars = numChars - 1
	end
	return str:sub(start, currentIndex - 1)
end

local TM2PlayerName = UnitName("player") or "Unknown"
	local currentSpec, currentSpecName
	
local unitGUIeventHandlers = {
	-- Handle any events that don't accept a unit argument
	["PLAYER_ENTERING_WORLD"] = function  (self, unit)
		TauntMasterGUI_UnitFrames_ResetUnitButton(self.unit, unit)
	end,
	["PARTY_MEMBERS_CHANGED"] = function  (self, unit)
		
		TauntMasterGUI_UnitFrames_ResetUnitButton(self.unit, unit)
	end,
	["RAID_ROSTER_UPDATE"] = function  (self, unit)
		
		TauntMasterGUI_UnitFrames_ResetUnitButton(self.unit, unit)
	end,
	["GROUP_ROSTER_UPDATE"] = function  (self, unit)
		
		TauntMasterGUI_UnitFrames_ResetUnitButton(self.unit, unit)
	end,
	["PLAYER_TARGET_CHANGED"] = function  (self, unit)
		if UnitIsUnit(unit, "target") then
			self.unit.selected:Show()
		else
			self.unit.selected:Hide()
		end
	end,
	-- Handle any events that accept a unit argument
	["UNIT_MAXHEALTH"] = function  (self, unit)
		if self.unit.dead ~= UnitIsDeadOrGhost(unit) then
			TauntMasterGUI_UnitFrames_ResetUnitButton(self.unit, unit)
		else
			self.unit.healthBar:SetMinMaxValues(0, UnitHealthMax(unit))
			self.unit.healthBar:SetValue(UnitHealth(unit))
		end
	end,
	["UNIT_HEALTH"] = function  (self, unit)
		if self.unit.dead ~= UnitIsDeadOrGhost(unit) then
			TauntMasterGUI_UnitFrames_ResetUnitButton(self.unit, unit)
		else
			self.unit.healthBar:SetValue(UnitHealth(unit))
		end
	end,
	["UNIT_POWER"] = function  (self, unit)
		if self.unit.dead ~= UnitIsDeadOrGhost(unit) then
			TauntMasterGUI_UnitFrames_ResetPowerBar(self.unit, unit)
		else
			self.unit.powerBar:SetValue(UnitPower(unit))
		end
	end,
	["UNIT_MAXPOWER"] = function  (self, unit)
		if self.unit.dead ~= UnitIsDeadOrGhost(unit) then
			TauntMasterGUI_UnitFrames_ResetPowerBar(self.unit, unit)
		else
			self.unit.powerBar:SetMinMaxValues(0, UnitPowerMax(unit))
			self.unit.powerBar:SetValue(UnitPower(unit))
		end
	end,
	["UNIT_NAME_UPDATE"] = function  (self, unit)
		TauntMasterGUI_UnitFrames_ResetPowerBar(self.unit, unit)
	end,
	["UNIT_PORTRAIT_UPDATE"] = function  (self, unit)
		TauntMasterGUI_UnitFrames_ResetUnitButton(self.unit, unit)
	end,
	["UNIT_THREAT_SITUATION_UPDATE"] = function  (self, unit)
		if not ( UnitIsDeadOrGhost(unit) ) then
			local status = UnitThreatSituation(unit)
			if status and status > 0 then
				local r, g, b = GetThreatStatusColor(status)
				self.unit.healthBar:SetStatusBarColor(r, g, b)
			else
				self.unit.healthBar:SetStatusBarColor(0, 1, 0)
			end
		else
			self.unit.healthBar:SetStatusBarColor(0.3, 0.3, 0.3)
		end
	end
}




function RaidFrames:OnInitialize()
	-- do init tasks here, like loading the Saved Variables, 
	-- or setting up slash commands.
end

function RaidFrames:OnEnable()
	-- Do more initialization here, that really enables the use of your addon.
	-- Register Events, Hook functions, Create Frames, Get information from 
	-- the game that wasn't available in OnInitialize
	
	private.CreateHeader()

	-- Create a Header to drive this
	CreateFrame("Frame", "TM2_Actionbutton_Header", UIParent, "SecureHandlerStateTemplate")
	RegisterStateDriver(TM2_Actionbutton_Header, "page", "[mod:ctrl]3;[mod:shift]2;[mod:alt]1;0")
	TM2_Actionbutton_Header:SetAttribute("_onstate-page", [[
		self:SetAttribute("state", newstate)
		control:ChildUpdate("state", newstate)
		]])

	for i=1, 5 do
		private.Create_Tm2_Actionbuttons(i)
	end
end

function RaidFrames:OnDisable()
	-- Unhook, Unregister Events, Hide frames that you created.
	-- You would probably only use an OnDisable if you want to 
	-- build a "standby" mode, or be able to toggle modules on/off.
end

function RaidFrames:SysMessage(msg)
	if(TM2debug) then
		self:Print("Debug: "..msg);
	end
end

function RaidFrames:GDHchatSpam(msg)
	if(TMSchatSpam) then
		self:Print(msg);
	end
end

function RaidFrames:Show_Hide()


end

function private.Create_Tm2_Actionbuttons(i)
	local currentSpecName = TM2.db.char.specialization
	local AB_Spells = TM2.db.char.Spells[currentSpecName]["TM2Button"..i]
	-- Create a button on the header
	local button = LibStub("LibActionButton-1.0"):CreateButton(i, "TM2_ActionButton" .. i, TM2_Actionbutton_Header)
	if i > 1 then
		button:SetPoint("LEFT", "TM2_ActionButton"..(i - 1 ), "RIGHT")
	end
	TM2_ActionButton1:SetPoint("CENTER", UIParent,100,100)
	button:Hide()
	--button:Show()
	button:DisableDragNDrop(false)

	button:SetAttribute("unit", "mouseover")
	button:SetAttribute("unitsuffix", "target")

	button:SetState( 0, AB_Spells[0]["type"], select(7, GetSpellInfo( AB_Spells[0]["spell"] )) )
	button:SetState( 1, AB_Spells[1]["type"], select(7, GetSpellInfo( AB_Spells[1]["spell"] )) )
	button:SetState( 2, AB_Spells[2]["type"], select(7, GetSpellInfo( AB_Spells[2]["spell"] )) )
	button:SetState( 3, AB_Spells[3]["type"], select(7, GetSpellInfo( AB_Spells[3]["spell"] )) )
	
end





function private.CreateHeader()
	local frame = CreateFrame("Frame", "TauntMasterUnitFrames_Header", UIParent, "SecureGroupHeaderTemplate")
	frame:SetMovable(true)
	frame:SetPoint("CENTER") 
	--frame:SetAttribute("name", value) - Sets a secure frame attribute
	frame:SetAttribute("template", "TauntMasterUnitFrames_UnitTemplate")
	frame:SetAttribute("templateType" , "Frame")

	frame:SetAttribute("point", "TOP")
	frame:SetAttribute("xOffset", -1)
	frame:SetAttribute("yOffset", 1)

	frame:SetAttribute("maxColumns" , TM2.db.char.gui.maxColumns)
	frame:SetAttribute("unitsPerColumn" , TM2.db.char.gui.unitsPerColumn)
	frame:SetAttribute("columnSpacing", 1)
	frame:SetAttribute("columnAnchorPoint", "LEFT")

	frame:SetAttribute("showRaid", TM2.db.char.gui.showRaid)
	frame:SetAttribute("showParty", TM2.db.char.gui.showParty)
	frame:SetAttribute("showPlayer" , TM2.db.char.gui.showPlayer)
	frame:SetAttribute("showSolo" , TM2.db.char.gui.showSolo)
	frame:SetAttribute("groupBy"   , "ASSIGNEDROLE")
	frame:SetAttribute("groupingOrder" , "MAINTANK, MAINASSIST, TANK, HEALER, DAMAGER, NONE")
	
	TauntMasterUnitFrames_Header:Show()
end

function TauntMasterGUI_UnitFrames_Frame_OnLoad(frame)
	-- Nudge the status bar frame levels down
	frame.unit.healthBar:SetFrameLevel(frame.unit:GetFrameLevel())
	frame.unit.powerBar:SetFrameLevel(frame.unit:GetFrameLevel())
	frame.unit:RegisterForDrag("LeftButton")
	frame.unit:RegisterForClicks("AnyUp")

	for event in pairs(unitGUIeventHandlers) do
		frame:RegisterEvent(event)
	end

	frame.unit:SetPoint("TOPLEFT", TM2.db.char.gui.width/100*(1/36 *100), -1*(TM2.db.char.gui.height/100 * (1/36 *100)))
	frame:SetWidth(TM2.db.char.gui.width)
	frame.unit:SetWidth(TM2.db.char.gui.width/100 * (34/36 *100))
	frame.unit.healthBar:SetWidth(TM2.db.char.gui.width/100 * (34/36 *100))
	frame.unit.powerBar:SetWidth(TM2.db.char.gui.width/100 * (34/36 *100))

	frame:SetHeight(TM2.db.char.gui.height)
	frame.unit:SetHeight(TM2.db.char.gui.height/100 * (34/36 *100))
	frame.unit.healthBar:SetHeight(TM2.db.char.gui.height/100 * (30/36 *100))
	frame.unit.powerBar:SetHeight(TM2.db.char.gui.height/100 * (3/36 *100))

	-- set atributes
	frame.unit:SetAttribute("useparent-unit", "true")
	for i =1 ,5 do
		frame.unit:SetAttribute("*type"..i, "macro")
		frame.unit:SetAttribute( "*macrotext"..i, "/click TM2_ActionButton"..i)
	end
end

function TauntMasterGUI_UnitFrames_Frame_OnShow(button)
	local unit = button:GetAttribute("unit")
	if unit then
		local guid = UnitGUID(unit)
		if guid ~= button.guid then
			TauntMasterGUI_UnitFrames_ResetUnitButton(button.unit, unit)
			button.guid = guid
		end
	end
end

function TauntMasterGUI_UnitFrames_Frame_OnEvent(self, event, arg1, ...)
	local unit = self:GetAttribute("unit")
	if not unit then
		return
	end
	local handler = unitGUIeventHandlers[event]
	if handler then
		handler(self, unit)
	end
end

function TauntMasterGUI_UnitFrames_OnUpdate(button, elapsed)
	local unit = button:GetAttribute("unit")
	TauntMasterGUI_oomicon(button, unit)
end

function TauntMasterGUI_UnitFrames_ResetName(button, unit)
	local name = UnitName(unit) or "Unknown"
    local class = select(2, UnitClass(unit)) or "WARRIOR"
	local classColor = RAID_CLASS_COLORS[class]
	button.name:SetTextColor(classColor.r, classColor.g, classColor.b)
	local substring
	for length=#name, 1, -1 do
		substring = utf8sub(name, 1, length)
		button.name:SetText(substring)
		if button.name:GetStringWidth() <= (button.name:GetWidth() - 6 ) then
			return
		end
	end
	--[[
	local status = UnitThreatSituation(unit)
	if status and status > 0 then
	local r, g, b = GetThreatStatusColor(status)
	button.healthBar:SetStatusBarColor(r, g, b)
	else
	button.healthBar:SetStatusBarColor(1, 1, 1)
	end
	]]
end

function TauntMasterGUI_UnitFrames_ResetHealthBar(button, unit)
	local alive = not UnitIsDeadOrGhost(unit)
	if alive then
		if status and status > 0 then
			local r, g, b = GetThreatStatusColor(status)
			button.healthBar:SetStatusBarColor(r, g, b)
		else
			button.healthBar:SetStatusBarColor(0, 1, 0)
		end
		button.healthBar:SetMinMaxValues(0, UnitHealthMax(unit))
		button.healthBar:SetValue(UnitHealth(unit))
		button.dead = false
	else
		button.healthBar:SetStatusBarColor(0.3, 0.3, 0.3)
		button.healthBar:SetMinMaxValues(0, 1)
		button.healthBar:SetValue(1)
		button.dead = true
	end
end

function TauntMasterGUI_UnitFrames_ResetPowerBar(button, unit)
	local power = UnitPower(unit)
	local powermax = UnitPowerMax(unit)
	local powerType, powerToken = UnitPowerType(unit)
	local powerColor = PowerBarColor[powerToken]
	local alive = not UnitIsDeadOrGhost(unit)
	if alive then
		button.powerBar:SetStatusBarColor(powerColor.r, powerColor.g, powerColor.b)
	else
		button.powerBar:SetStatusBarColor(0.3, 0.3, 0.3)
		--button.unit.oomicon:Hide()
	end
	button.powerBar:SetMinMaxValues(0, powermax)
	button.powerBar:SetValue(power)
	button.oomicon:Hide()
end

function TauntMasterGUI_UnitFrames_ResetUnitButton(button, unit)
	TauntMasterGUI_UnitFrames_ResetHealthBar(button, unit)
	TauntMasterGUI_UnitFrames_ResetPowerBar(button, unit)
	TauntMasterGUI_UnitFrames_ResetName(button, unit)
	if UnitIsUnit(unit, "target") then
		button.selected:Show()
	else
		button.selected:Hide()
	end
	local status = UnitThreatSituation(unit)
	if status and status > 0 then
		local r, g, b = GetThreatStatusColor(status)
		button.healthBar:SetStatusBarColor(r, g, b)
	else
		local alive = not UnitIsDeadOrGhost(unit)
		if alive then
			button.healthBar:SetStatusBarColor(0, 1, 0)
		else
			button.healthBar:SetStatusBarColor(0.3, 0.3, 0.3)
		end
	end
end



function TauntMasterGUI_UnitFrames_Button_OnDragStart(self, button)
	TauntMasterUnitFrames_Header:StartMoving()
	TauntMasterUnitFrames_Header.isMoving = true
end

function TauntMasterGUI_UnitFrames_Button_OnDragStop(self, button)
	if TauntMasterUnitFrames_Header.isMoving then
		TauntMasterUnitFrames_Header:StopMovingOrSizing()
	end
end

function TauntMasterGUI_oomicon(self, unit)
	local unitsClass = select(2, UnitClass(unit))
	if unitsClass == "PALADIN" or unitsClass == "PRIEST" or unitsClass == "SHAMAN" or unitsClass == "DRUID" or unitsClass == "WARLOCK"  or unitsClass == "MAGE" then
		local power = UnitPower(unit)
		local maxpower = UnitPowerMax(unit)
		local powerType = UnitPowerType(unit)
		if powerType == 0 then
			if power <= (maxpower * (30 / 100)) then
				self.unit.oomicon:Show()
			else
				self.unit.oomicon:Hide()
			end
		else
			self.unit.oomicon:Hide()
		end
	else
		self.unit.oomicon:Hide()
	end
end




