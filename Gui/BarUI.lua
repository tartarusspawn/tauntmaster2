local L = LibStub("AceLocale-3.0"):GetLocale("TauntMaster2") -- loads the localization table
-- =================================================================================
--                          TauntMaster2                                          ==
--          http://www.curse.com/addons/wow/taunt-master-2                        ==
-- =================================================================================
-- Author      : Tartarusspawn
	-- Print a message to the chat frame
	--TM2:Print("msg")
local TM2 = select(2, ...)
local BarUI =  TM2:NewModule("BarUI", "AceEvent-3.0", "AceConsole-3.0", "AceHook-3.0")
local Util = TM2:GetModule("Util")
local private = {NUM_BUTTONS=5, NUMS_STATES = 3}

function BarUI:OnInitialize()
	BarUI:SysMessage("BarUI:OnInitialize()")
	-- do init tasks here, like loading the Saved Variables, 
	-- or setting up slash commands.
end

function BarUI:OnEnable()
	BarUI:SysMessage("BarUI:OnEnable()")
	head = CreateFrame("Frame", "TM2_Player_Actionbutton_Header", TauntMasterUnitFrames_Header, "SecureHandlerStateTemplate")
	RegisterStateDriver( head, "page", "[mod:ctrl]3;[mod:shift]2;[mod:alt]1;0")
	head:SetAttribute("_onstate-page", [[
		self:SetAttribute("state", newstate)
		control:ChildUpdate("state", newstate)
		]])
	for j = 1, private.NUM_BUTTONS do
		BarUI:createActionButton(j, head)
	end
end

function BarUI:SysMessage(msg)
	if(TM2debug) then
		self:Print("Debug: "..msg);
	end
end

function BarUI:GDHchatSpam(msg)
	if(TMSchatSpam) then
		self:Print(msg);
	end
end



function BarUI:createActionButton(i , header)
	local currentSpecName = TM2.db.char.specialization
	local AB_Spells = TM2.db.char.Spells[currentSpecName]["TM2Button"..i]
	-- Create a button on the header
	local button = LibStub("LibActionButton-1.0"):CreateButton(i, "TM2_player_ActionButton" .. i, header)
	if i > 1 then
		button:SetPoint("LEFT", "TM2_player_ActionButton"..(i - 1 ), "RIGHT")
	end
	TM2_player_ActionButton1:SetPoint("BOTTOMLEFT", TauntMasterUnitFrames_Header, "TOPLEFT", offsetX, 0)
	button:Show()
	--button:DisableDragNDrop(true)
	button:DisableDragNDrop(false)


	button:SetState( 0, AB_Spells[0]["type"], select(7, GetSpellInfo(AB_Spells[0]["spell"])) )
	button:SetState( 1, AB_Spells[1]["type"], select(7, GetSpellInfo(AB_Spells[1]["spell"])) )
	button:SetState( 2, AB_Spells[2]["type"], select(7, GetSpellInfo(AB_Spells[2]["spell"])) )
	button:SetState( 3, AB_Spells[3]["type"], select(7, GetSpellInfo(AB_Spells[3]["spell"])) )
end





